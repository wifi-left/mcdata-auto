# 关于仓库
## Minecraft 数据搜集
🔎这是一个收集一些 Minecraft 数据的仓库（For Developers），方便开发人员开发。🔎 This is a repository used to collect data in Minecraft. (For Developers)

## 介绍

GitHub 自动化同步到这里。

源代码： https://github.com/wifi-left/auto-mcdata

## 如何使用
API： `https://gitlab.com/wifi-left/mcdata-auto/-/raw/master/info.json`

`supportVersions` 为支持的版本号。

使用 `https://gitlab.com/wifi-left/mcdata-auto/-/raw/master/files/{版本号}/数据名称` 即可下载对应数据。

数据存储在 [./files](./files)
